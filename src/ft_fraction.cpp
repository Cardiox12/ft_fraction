/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_fraction.cpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbellavi <bbellavi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/08 23:37:00 by bbellavi          #+#    #+#             */
/*   Updated: 2019/12/09 10:43:17 by bbellavi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_fraction.h>

using namespace std;

Fraction::Fraction(int _numerator, int _denominator) : numerator(_numerator), denominator(_denominator)
{
    assert(_denominator != 0);
}
Fraction::Fraction(const Fraction &other) : numerator(other.numerator), denominator(other.denominator) { };
Fraction::Fraction(int _numerator) : numerator(_numerator), denominator(1) { };

void        Fraction::show(ostream &stream) const
{
    if (denominator != 1)
        stream << numerator << " / " << denominator;
    else
        stream << numerator;
}

bool        Fraction::is_reductible() const
{
    int res = pgcd();

    return (!(res == 1 || res == denominator));
}

int         Fraction::pgcd() const
{
    int a = denominator;
    int b = numerator;

    int c = 1;

    if (b == 0)
        return (a);
    while (b != 0)
    {
        c = a % b;
        a = b;
        b = c;
    }
    return (a);
}

void        Fraction::reduce()
{
    int factor;
    
    if (is_reductible())
    {
        factor = pgcd();
        cout << "pgcd : (" << numerator << " ; " << denominator << ") = " << factor << endl << endl;
        numerator /= factor;
        denominator /= factor;
    }
}

Fraction   Fraction::operator*=(const Fraction &to_mul)
{

    denominator *= to_mul.denominator;
    numerator *= to_mul.numerator;

    reduce();
    return (*this);
}

Fraction    Fraction::operator+=(const Fraction &to_add)
{
    Fraction lfraction(to_add);
    int factor_1 = denominator;
    int factor_2 = to_add.denominator;

    reduce();
    lfraction.reduce();
    if (factor_1 != factor_2)
    {
        numerator *= factor_2;
        denominator *= factor_2;

        lfraction.numerator *= factor_1;
        lfraction.denominator *= factor_1;
        
        cout << "Numerator :" << lfraction.numerator << endl;
        cout << "Denominator : " << lfraction.denominator << endl;

        reduce();
        lfraction.reduce();
        numerator += lfraction.numerator;
    }
    else
        numerator += to_add.numerator;
    reduce();
    return (*this);
}

ostream     &operator<<(ostream &stream, const Fraction &fraction)
{
    fraction.show(stream);
    return (stream);
}